package com.test.api;

import com.test.model.Transaction;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by nikolay on 06.07.17.
 */

public interface API {

        @GET("history")
        Call<List<Transaction>> getData(@Query("userid") String userId, @Query("datefrom") long dateFrom, @Query("dateto") long dateTo);

}
