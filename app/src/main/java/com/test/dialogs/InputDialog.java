package com.test.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.test.R;
import com.test.listeners.InputDialogListener;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by nikolay on 06.07.17.
 */

public class InputDialog extends Dialog implements View.OnClickListener

{

    private String userId;
    private long dateFrom;
    private long dateTo;
    private Context context;
    private EditText userIdEditText;
    private EditText dateFromEditText;
    private EditText dateToEditText;
    private DatePickerDialog datePickerDialog;
    private InputDialogListener inputDialogListener;

    public InputDialog(Context context, InputDialogListener inputDialogListener) {

        super(context);
        this.context = context;
        this.inputDialogListener = inputDialogListener;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fields_dialog);
        setTitle("Enter fields");

        ((Button) findViewById(R.id.sendFieldsButton)).setOnClickListener(this);
        ((Button) findViewById(R.id.cancelButton)).setOnClickListener(this);
        userIdEditText = (EditText) findViewById(R.id.userIdEditText);
        dateFromEditText = (EditText) findViewById(R.id.dateFromEditText);
        dateFromEditText.setFocusable(false);
        dateFromEditText.setOnClickListener(this);
        dateToEditText = (EditText) findViewById(R.id.dateToEditText);
        dateToEditText.setFocusable(false);
        dateToEditText.setOnClickListener(this);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.cancelButton:
                if(inputDialogListener != null) {
                    inputDialogListener.onCancel();
                }
                break;
                case R.id.sendFieldsButton:
                if(inputDialogListener != null){
                    inputDialogListener.onInput(userId, dateFrom, dateTo);
                }
                break;

            case R.id.dateFromEditText:
                datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        dateFromEditText.setText(String.format("%02d.%02d.%04d", dayOfMonth, monthOfYear + 1, year));
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        dateFrom = calendar.getTimeInMillis() / android.text.format.DateUtils.SECOND_IN_MILLIS;
                        ;
                    }
                }, 2017, 1, 1);
                datePickerDialog.show();
                break;

            case R.id.dateToEditText:
                datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        dateToEditText.setText(String.format("%02d.%02d.%04d", dayOfMonth, monthOfYear + 1, year));
                        Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);
                        dateTo = calendar.getTimeInMillis() / android.text.format.DateUtils.SECOND_IN_MILLIS;
                        ;
                    }
                }, 2017, 1, 1);
                datePickerDialog.show();
                break;

        }
    }
}
