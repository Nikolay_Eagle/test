package com.test.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nikolay on 06.07.17.
 */

public class Transaction  {

        @SerializedName("TransactionID")
        @Expose
        private String transactionID;
        @SerializedName("amount")
        @Expose
        private String amount;
        @SerializedName("Debit")
        @Expose
        private String debit;
        @SerializedName("Credit")
        @Expose
        private String credit;
        @SerializedName("Date")
        @Expose
        private String date;

        public String getTransactionID() {
            return transactionID;
        }

        public void setTransactionID(String transactionID) {
            this.transactionID = transactionID;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDebit() {
            return debit;
        }

        public void setDebit(String debit) {
            this.debit = debit;
        }

        public String getCredit() {
            return credit;
        }

        public void setCredit(String credit) {
            this.credit = credit;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

    }
