package com.test.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.test.App;
import com.test.R;
import com.test.model.Transaction;
import com.test.adapters.TransactionsAdapter;
import com.test.dialogs.InputDialog;
import com.test.listeners.InputDialogListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements InputDialogListener, SwipeRefreshLayout.OnRefreshListener {

    private ProgressBar progressBar;
    private RecyclerView transactionList;
    private InputDialog inputDialog;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noResultTextView;

    private String userId;
    private long dateFrom;
    private long dateTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        openInputDialog();
    }

    private void initViews() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        transactionList = (RecyclerView) findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(MainActivity.this);
        noResultTextView = (TextView) findViewById(R.id.noResultTextView);
    }

    private void isVisibleProgressbar(boolean isVisible) {
        if (progressBar != null) {
            if (isVisible) {
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressBar.setVisibility(View.GONE);
            }
        }
    }

    private void openInputDialog() {
        inputDialog = new InputDialog(this, this);
        inputDialog.show();
        inputDialog.setCancelable(false);
    }


    @Override
    public void onInput(String userId, long dateFrom, long dateTo) {
        if (inputDialog != null)
            inputDialog.dismiss();
        this.userId = userId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        loadList(userId, dateFrom, dateTo);
        setVisibilityNoResultView(false);
        isVisibleProgressbar(true);
    }

    @Override
    public void onCancel() {
        finish();
    }


    private void loadList(String userId, long dateFrom, long dateTo) {
        App.getApi().getData(userId, dateFrom, dateTo).enqueue(new Callback<List<Transaction>>() {
            @Override
            public void onResponse(Call<List<Transaction>> call, Response<List<Transaction>> response) {
                isVisibleProgressbar(false);
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() != null) {
                    List<Transaction> transactions = new ArrayList<Transaction>();
                    transactions.addAll(response.body());
                    initTransactionList(transactions);
                } else {
                    setVisibilityNoResultView(true);
                }
            }

            @Override
            public void onFailure(Call<List<Transaction>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
                isVisibleProgressbar(false);
                setVisibilityNoResultView(true);
                Toast.makeText(MainActivity.this, "Error loading data! ", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setVisibilityNoResultView(boolean isVisible) {
        if (isVisible) {
            noResultTextView.setVisibility(View.VISIBLE);
        } else {
            noResultTextView.setVisibility(View.GONE);
        }
    }

    private void initTransactionList(List<Transaction> transactions) {
        if (transactions.size() > 0) {
            TransactionsAdapter transactionAdapter = new TransactionsAdapter(MainActivity.this, transactions);
            transactionList.setLayoutManager(new LinearLayoutManager(MainActivity.this));
            transactionList.setAdapter(transactionAdapter);
        } else {
            setVisibilityNoResultView(true);
        }
    }

    @Override
    public void onRefresh() {
        loadList(userId, dateFrom, dateTo);
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case R.id.repeat:

                openInputDialog();
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
