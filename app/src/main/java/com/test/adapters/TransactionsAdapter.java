package com.test.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.test.R;
import com.test.model.Transaction;

import java.util.List;

/**
 * Created by nikolay on 06.07.17.
 */

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.TransactionHolder> {

    private Context context;
    private List<Transaction> transactionList;

    public TransactionsAdapter(Context context, List<Transaction> transactionList) {
        this.context = context;
        this.transactionList = transactionList;

    }

    @Override
    public TransactionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.transaction_list_item, null, false);
        return new TransactionHolder(view);
    }

    @Override
    public void onBindViewHolder(TransactionHolder holder, int position) {
        holder.transactionID.setText(transactionList.get(position).getTransactionID() != null ? transactionList.get(position).getTransactionID() : "" );
        holder.amount.setText(transactionList.get(position).getAmount() != null ? transactionList.get(position).getAmount() : "" );
        holder.debit.setText(transactionList.get(position).getDebit() != null ? transactionList.get(position).getDebit() : "");
        holder.credit.setText(transactionList.get(position).getCredit() != null ? transactionList.get(position).getCredit() : "");
        holder.date.setText(transactionList.get(position).getDate() != null ? transactionList.get(position).getDate() : "");

    }

    @Override
    public int getItemCount() {
        return transactionList.size();
    }

    class TransactionHolder extends RecyclerView.ViewHolder {
        private TextView transactionID;
        private TextView amount;
        private TextView debit;
        private TextView credit;
        private TextView date;

        public TransactionHolder(View itemView) {
            super(itemView);

            transactionID = (TextView) itemView.findViewById(R.id.transactionID);
            amount = (TextView) itemView.findViewById(R.id.amount);
            debit = (TextView) itemView.findViewById(R.id.debit);
            credit = (TextView) itemView.findViewById(R.id.credit);
            date = (TextView) itemView.findViewById(R.id.date);
        }
    }
}
