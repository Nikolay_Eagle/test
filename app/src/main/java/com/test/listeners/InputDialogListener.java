package com.test.listeners;

/**
 * Created by nikolay on 06.07.17.
 */

public interface InputDialogListener {

    void onInput(String userId, long dateFrom, long dateTo);
    void onCancel();
}
